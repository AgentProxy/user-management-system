# user-management-system

### Project Description
This is a full-stack user management system app where one can view, add, edit and delete users. Vue is the main front-end framework used with Bulma for the CSS framework. For the backend, the database used for this app was MongoDB. The backend stack used was Node, Express, and Mongoose. This app is created by Eric Flores to be submitted for the coding test.


### Features Implemented
1. Retrieving of users and displaying them on a table
2. Adding a single user
3. Deleting a single user
4. Editing a single user details

### To start the app.
```
1. Install the required packages:
npm install

2. Start the vue app
npm run serve

3. Open the 'backend' folder:
cd backend

4. Install the required packages for the backend:
npm install

5. Start the mongodb server.
mongo

6. Start the node server on http://localhost:4000/api 
nodemon

```