const User = require('../models/User')

module.exports = (router) => {
  router.post('/editUser/:id', async (req,res) => {
      // Create new user and return user data for display
      try{
        // Make sure that req.body is present
        if(!req.body){
        throw new Error('Incomplete parameters passed.');
      } else {
        const user = await User.findByIdAndUpdate(req.params.id, {
          $set: req.body
        })
        res.status(201).json(user)
      }
    } catch(err) {
      res.status(400).json({message: err.message})
    }
  });
}