const User = require('../models/User');

module.exports = (router) => {
  router.get('/getUsers', async (req,res) => {
    try{
    const users = await User.find({}, {lean: true})
      .select('id firstName lastName billingAddress deliveryAddress');
    // Return all users
    res.json(users)
    } catch(err) {
      res.status(400).json({message: err.message})
    }
  })
  
}