const User = require('../models/User');

module.exports = (router) => {
  router.get('/getUsersTableData', async (req,res) => {
    try{
    const users = await User.find({}, {lean: true})
      .select('id firstName lastName billingAddress deliveryAddress');
    
    const tableHeaders = [{
        headerName: 'First Name',
        value: 'firstName'
      },
      {
        headerName: 'Last Name',
        value: 'lastName'
      },
      {
        headerName: 'Delivery Address',
        value: 'deliveryAddress'
      },
      {
        headerName: 'Billing Address',
        value: 'billingAddress'
      },
      {
        headerName: 'Actions',
        value: 'actions'
      },
    ];
    // Return all users
    res.json({
      tableHeaders,
      tableData: users
    })
    } catch(err) {
      res.status(400).json({message: err.message})
    }
  })
  
}