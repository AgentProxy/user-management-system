const User = require('../models/User')

module.exports = (router) => {
  router.post('/deleteUser/:id', async (req,res) => {
    // Create new user and return user data for display
    try{
      const data = await User.findByIdAndRemove(req.params.id);
      // To check if a user was successfully deleted
      if(data){
        res.status(200).json(data)
      }
      else{
        throw new Error();
      }
    } catch(err) {
      res.status(400).json({message: err.message})
    }
  });
}