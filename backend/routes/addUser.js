const User = require('../models/User')

module.exports = (router) => {
  router.post('/addUser', async (req,res) => {
    // Create new user and return user data for display
    try{
      // Make sure that all parameters are complete
      if(!req.body || !req.body.firstName || !req.body.lastName || 
          !req.body.billingAddress || !req.body.deliveryAddress){
        throw new Error('Incomplete parameters passed.');
      } else {
        const user = new User({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          billingAddress: req.body.billingAddress,
          deliveryAddress: req.body.deliveryAddress,
        })
        const newUser = await user.save()
        res.status(201).json(newUser)
      }
    } catch(err) {
      res.status(400).json({message: err.message})
    }
  })
}