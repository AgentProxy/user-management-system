const express = require('express');
const router = express.Router()

require('./addUser')(router);
require('./deleteUser')(router);
require('./editUser')(router);
require('./getUsers')(router);
require('./getUsersTableData')(router);

module.exports = router;



