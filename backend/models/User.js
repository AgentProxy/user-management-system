const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  firstName: {type: String, required: true},
  lastName: {type: String, required: true},
  billingAddress: {type: String, required: true},
  deliveryAddress: {type: String, required: true}
}, { id: false });

module.exports = mongoose.model('User', UserSchema);