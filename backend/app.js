const cors = require('cors');
const express = require('express');
const app = express();
// To enable cors
app.use(cors());

app.use(express.json());

const api = require('./routes/router');

app.use('/api', api);

require('./utils/dbConnect')

// Create port, set to 4000 by default
const port = process.env.PORT || 4000;

const server = app.listen(port, () => {
  console.log('Connected to port ' + port)
})
// For handling errors
app.use(function (err, req, res, next) {
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});