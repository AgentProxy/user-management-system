import axios from "axios";

const state = () => {
  return { 
    isLoading: false,
    selectedUser: null,
    showUserModal: false,
    userModalOperation: '',
    usersTableData: null
  };
};

const mutations = {
  SET_SELECTED_USER: function(state, selectedUser){
    state.selectedUser = selectedUser;
  },
  SET_SHOW_USER_MODAL: function(state, showUserModal){
    state.showUserModal = showUserModal
  },
  SET_USER_MODAL_OPERATION: function(state, userModalOperation){
    state.userModalOperation = userModalOperation
  },
  SET_IS_LOADING: function(state, isLoading){
    state.isLoading = isLoading;
  },
  SET_USERS_TABLE_DATA: function (state, usersTableData) {
    state.usersTableData = usersTableData;
  },
};

const actions = {
  addUser: function ({dispatch}, payload) {
    axios
    .post(
      "http://localhost:4000/api/addUser",
      payload
    )
    .then(() => {
      dispatch('closeUserModal');
      dispatch('getUsersTableData');
    }).catch(() => {
      alert('An error occured when adding new user!');
    })
  },
  deleteUser: function({dispatch}, payload) {
    axios
    .post(
      "http://localhost:4000/api/deleteUser/" + payload.userId    
    )
    .then(() => {
      dispatch('getUsersTableData');
    }).catch(() => {
      alert('An error occured when deleting user!');
    })
  },
  editUser: function ({dispatch}, payload) {
    axios
    .post(
      "http://localhost:4000/api/editUser/" + payload.userId,
      payload
    )
    .then(() => {
      dispatch('closeUserModal');
      dispatch('getUsersTableData');
    }).catch(() => {
      alert('An error occured when editing new user!');
    })
  },
  getUsersTableData: function ({ commit }) {
    commit('SET_IS_LOADING', true);
    axios
      .get(
        "http://localhost:4000/api/getUsersTableData"
      )
      .then((response) => {
        if (response && response.data) {
          commit("SET_USERS_TABLE_DATA", response.data);
        }
      }).catch(() => {
        alert('An error occured!');
      }).then(() => {
        commit('SET_IS_LOADING', false);
      });
  },
  closeUserModal: function({commit}){
    commit('SET_SELECTED_USER', null);
    commit('SET_USER_MODAL_OPERATION', '');
    commit('SET_SHOW_USER_MODAL', false);
  },
  showUserModal: function({commit}, payload){
    if(payload && payload.operation){
      // For editing
      if(payload.selectedUser){
        commit('SET_SELECTED_USER', payload.selectedUser);
      }
      commit('SET_USER_MODAL_OPERATION', payload.operation);
      commit('SET_SHOW_USER_MODAL', true);
    }
  }
};

// Export in format that can easily be added as a Vuex module
export default {
  users: {
    // Set modules to namespaced equals true by default
    namespaced: true,
    state,
    actions,
    mutations,
  },
};
